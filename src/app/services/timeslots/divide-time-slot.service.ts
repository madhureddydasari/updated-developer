import { Injectable } from '@angular/core';
import { concat } from 'rxjs';
import { stringify } from 'querystring';
import { map } from 'rxjs/operators';
import { StringMap } from '@angular/compiler/src/compiler_facade_interface';

@Injectable({
  providedIn: 'root'
})
export class DivideTimeSlotService {
  
  constructor() { }

  ngOnInit() {
  }

  generateTimes()
  {let kat=[];
    for(let k=0;k<7;k++)
    {
      kat.push( k.toString() )
    }
    return kat
  }

  generateIntervals()
  {
    let kat:number[]=[];
    for(let k=0;k<7;k++)
    {
      kat.push( parseInt("15".toString()) )
    }
    return kat
  }

  
  divideTimeSlots(startTime: string[], endTime: string[],intervals: number[])
  {
    let s1,s2,s3,s4,s5,s6,s7 : string
    let e1,e2,e3,e4,e5,e6,e7 : string

      let weeks =[];  // stores data of wekkdays by 
      let times_ara: any[];  //stores data i.e. no. of slots of a day 
    // let B=new Array()
    let days=["day0","day1","day2","day3","day4","day5","day6"]
    let startTimes=[s1,s2,s3,s4,s5,s6,s7]  // //array of 7 start times each of a particular week
    let endTimes=[e1,e2,e3,e4,e5,e6,e7]   //array of 7 end times each of a particular week
    //you can use these in for loop instead of i and generate time slots of a week with each days startTime and endTime 
    //here k is to provide slot ids to generate them according to week
    for(let j=0,k=j+1;j<7;j++,k++)
    {    
      let start_time = this.parseTime(startTime[j]);
      let end_time = this.parseTime(endTime[j]);
      let interval = parseInt(intervals[j].toString()) // as it is a string array , we convert each of it to number or else it(i.e. interval) gets appended and stops at the begining itself
      // console.log("start and end time , interval is :"+typeof(start_time),typeof(end_time),typeof(interval))
      times_ara= this.calculate_time_slot( start_time, end_time , interval,k);
      weeks.push({slots : times_ara})
    }
    return weeks
//********************this changed my logic completely  to move forward*/


 // sharefunction()
  // {
  //   let B=new Array()
  //   for(let j=0;j<=10;j++)
  //     {    
  //     // let A=new Array()
  //     // for(let i=0;i<6;i++)
  //     // {
  //     // A.push(i) 
  //     // }
  //     B.push({j})
  //     }
  //   return B
  // }


  }

  parseTime(s: string) {
    var c = s.split(':');
    return parseInt(c[0]) * 60 + parseInt(c[1]);
  }

  convertHours(mins: number)
  {
    let hour = Math.floor(mins/60);
    let mins2 = mins%60;
    var converted = this.pad(hour, 2)+':'+this.pad(mins2, 2);
    return converted;
  }

  pad(str: string | number | any[], max: number) {
    str = str.toString();
    return str.length < max ? this.pad("0" + str, max) : str;
  }

  calculate_time_slot(start_time: number, end_time: number, interval: number,k: number){
      // var i, formatted_time;
    let time_slots = new Array();
      for(let i=start_time,j=1; i<=end_time-interval; i = i+interval,j++){
      let slotTime1 = this.convertHours(i);
      let slotTime2 = this.convertHours(i+interval);
      let slotTime = slotTime1+" - "+slotTime2
      let slotId =k*1000+j //which stores slot id to call the id out to the user by the reception on day of visit
      let slotDateArray : string[]=[]; 

      // let date1=new Date().getTime().toString()
      // slotDateArray.push(date1)
      // let date2 = new Date().toLocaleDateString()
// new Date().toLocaleTimeString() returns tne exact time *********ONLY TIME No DATE is returned
      // slotDateArray.push(date2)

      let slotStatus :string = "available"
      time_slots.push({slotId,slotTime,slotStatus,slotDateArray});
      
    }
    return time_slots;
  }

  // slotDateArray is an Array
  // similarly 


  // sharefunction()
  // {
  //   let B=new Array()
  //   for(let j=0;j<=10;j++)
  //     {    
  //     // let A=new Array()
  //     // for(let i=0;i<6;i++)
  //     // {
  //     // A.push(i) 
  //     // }
  //     B.push({j})
  //     }
  //   return B
  // }


}









