import { TestBed } from '@angular/core/testing';

import { DivideTimeSlotService } from './divide-time-slot.service';

describe('DivideTimeSlotService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DivideTimeSlotService = TestBed.get(DivideTimeSlotService);
    expect(service).toBeTruthy();
  });
});
