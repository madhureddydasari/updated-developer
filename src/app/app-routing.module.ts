import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'developerlogin', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'view-doctor/:id',
    loadChildren: () => import('./view-doctor/view-doctor.module').then( m => m.ViewDoctorPageModule)
  },

  {
    path: 'update-doctor/:id',
    loadChildren: () => import('./update-doctor/update-doctor.module').then( m => m.UpdateDoctorPageModule)
  },

  {
    path: 'doctregister',
    loadChildren: () => import('./doctregister/doctregister.module').then( m => m.DoctregisterPageModule)
  },

  {
    path: 'developerlogin',
    loadChildren: () => import('./developerlogin/developerlogin.module').then( m => m.DeveloperloginPageModule)
  },
  
  {
    path: 'update-doctor-slots/:id',
    loadChildren: () => import('./update-doctor-slots/update-doctor-slots.module').then( m => m.UpdateDoctorSlotsPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
