import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeveloperloginPage } from './developerlogin.page';

const routes: Routes = [
  {
    path: '',
    component: DeveloperloginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeveloperloginPageRoutingModule {}
