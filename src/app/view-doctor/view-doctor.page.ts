import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Doctorstruct } from '../modal/Docotor';
import { FirebaseService } from '../services/firebase.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-view-doctor',
  templateUrl: './view-doctor.page.html',
  styleUrls: ['./view-doctor.page.scss'],
})
export class ViewDoctorPage implements OnInit {

  daysNames :string[]=["sun","mon","tue","wed","thu","fri","sat"]

  doctor : Doctorstruct ={
    name:'',
    description:'',
    department:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
    createdAt: '',
    // docId : '',

  }

  // books : bookingstruct ={
  //   userID : "",
  //   doctorid : "",
  //   doctorname : "" ,
  //   status : "" ,
  // }
  

  doctorsdb : Observable<Doctorstruct>
  timeSlots: string;
  day: number;

  constructor( private toastCtrl : ToastController,private fbservice : FirebaseService, private activatedRoute : ActivatedRoute , private router : Router) 
  {
    this.day=2
   }

  async ngOnInit() {
    const id= this.activatedRoute.snapshot.paramMap.get('id');
    if(id)
    {
      this.fbservice.getDoctor(id).subscribe(docData => {
        this.doctor = docData;        
      });
      
    } 

   
  }

  // ionViewDidLoad(){
  //   const id= this.activatedRoute.snapshot.paramMap.get('id');
  //   if(id)
  //   {
  //     this.fbservice.getDoctor(id).subscribe(docData => {
  //       this.doctor = docData;
  //     });
  //   } 

  // }

  getInnerHtmlData(){
    let data=""
    if(this.doctor.doctorExist == true ){data ="disable doctor"}
    else { data="enable Doctor"}

    // this.fbservice.getDoctor(this.doctor.id).subscribe(docData => {
    //   this.doctor = docData;

    return data
  }

  // deleteNote(){
  //   this.fbservice.deleteDoctor(this.doctor.id).then(() => {
  //     this.router.navigateByUrl('/');
  //   });
  // }

  // book(){
  //   this.books.doctorid = this.doctor.docsidd,
  //   this.books.doctorname = this.doctor.name ,
  //   this.books.status = "not visited" ,

  //   this.fbservice.bookDoctor(this.books).then(() => {
  //     this.toastCtrl.create({
  //       message : " booking confirmed"+ this.books.id,
  //       duration : 2000
  //     }).then((toast) => {
  //       toast.present()
  //     })
  // })
  // }

  
}


