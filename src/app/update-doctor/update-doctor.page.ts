// import { Component, OnInit } from '@angular/core';
// import { Doctorstruct } from '../modal/Docotor';
// import { ActivatedRoute, Router } from '@angular/router';
// import { FirebaseService } from '../services/firebase.service';
// import { DivideTimeSlotService } from '../services/timeslots/divide-time-slot.service';
// import { AlertController } from '@ionic/angular';
 
// @Component({
//   selector: 'app-update-doctor',
//   templateUrl: './update-doctor.page.html',
//   styleUrls: ['./update-doctor.page.scss'],
// })
// export class UpdateDoctorPage implements OnInit {


//   doctor : Doctorstruct ={
//     // docId:'',
//     name:'',
//     department:'',
//     description:'',
//     phone: '',
//     specialization: '',
//     consultationFee : '',
//     timings:'',
//     facilities:'',
//     startTime:'',
//     endTime:'',
//     interval:null,
//   }
  

//   constructor(private alertCtrl : AlertController,private fbtimeservice : DivideTimeSlotService,private activatedRoute : ActivatedRoute , private fbservice : FirebaseService , private router : Router) { }

//   ngOnInit() {
//     const id= this.activatedRoute.snapshot.paramMap.get('id');
//     if(id)
//     {
//       this.fbservice.getDoctor(id).subscribe(docData => {
//         this.doctor = docData 
//       });
//     }
//   }
  
//   // change(){
//   //   this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable)
//   // }

//  async updateDoctor()
//   {
//     // try
//     // {  
//         // try{
//           if(this.doctor.startTime && this.doctor.endTime && this.doctor.interval)
//           {
//             this.showAlert("reached service")
//             this.doctor.timeSlots = this.fbtimeservice.divideTimeSlots(this.doctor.startTime , this.doctor.endTime,this.doctor.interval)
//             // this.doctor.timeSlots = this.fbtimeservice.divideTimeSlots(this.doctor.startTime , this.doctor.endTime,this.doctor.interval)
          
//           }
//         // }
//         // catch(err)
//         // {
//         //   this.showAlert("rejected from service or condition"+err.message)
//         // }



//         // this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable) 
//         // no need of a SEPARATE piece of code as we merged that with the doctor struct and directly updating
//         await this.fbservice.updateDoctor(this.doctor).then(()=> {
//           this.router.navigateByUrl(`/view-doctor/${this.doctor.id}`)
//         });
//   //   }parseInt
//   // catch(err)
//   // {
//   //   if(err.code = "invalid-argument"){
//   //     this.showAlert("please fill StartTime , EndTime and interval to generate TimeSlots ")

//   //   }
//   //   else{
//   //     this.showAlert("error code is : "+err.code)
//   //   }
//   // }

//   }

//   async showAlert(message : string)
//   {
//     const alert = await this.alertCtrl.create ({
//       header:'Warning',
//       message ,
//       buttons:["OK"]
//     })
//     alert.present()
//   }

// }


import { Component, OnInit } from '@angular/core';
import { Doctorstruct } from '../modal/Docotor';
import { AlertController } from '@ionic/angular';
import { DivideTimeSlotService } from '../services/timeslots/divide-time-slot.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseService } from '../services/firebase.service';

@Component({
  selector: 'app-update-doctor',
  templateUrl: './update-doctor.page.html',
  styleUrls: ['./update-doctor.page.scss'],
})
export class UpdateDoctorPage implements OnInit {

  daysNames :string[]=["sun","mon","tue","wed","thu","fri","sat"]
  
  doctor : Doctorstruct ={
    // docId:'',
    name:'',
    department:'',
    description:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
  }

  constructor(private alertCtrl : AlertController,private fbtimeservice : DivideTimeSlotService,private activatedRoute : ActivatedRoute , private fbservice : FirebaseService , private router : Router) 
  { }

  ngOnInit() {
    const id= this.activatedRoute.snapshot.paramMap.get('id');
    if(id)
    {
      this.fbservice.getDoctor(id).subscribe(docData => {
        this.doctor = docData 
      });
    }
  }
  
  // change(){
  //   this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable)
  // }

  async updateDoctor()
  {

        try
        {
          // if(this.doctor.startTimings && this.doctor.endTimings && this.doctor.timingIntervals)
          // {
            // this.doctor.timeSlots = this.fbtimeservice.divideTimeSlots(this.doctor.startTimings , this.doctor.endTimings,this.doctor.timingIntervals)
            // console.log(this.doctor.timeSlots[0].slots[0])
            // this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable) 
            // no need of a SEPARATE piece of code as we merged that with the doctor struct and directly updating
            this.fbservice.updateDoctor(this.doctor).then(()=> {
              this.router.navigateByUrl(`/view-doctor/${this.doctor.id}`)
            });
          // }
          // else
          // {
            // this.showAlert("please fill StartTime , EndTime and interval with minimum of 15 mins to generate TimeSlots")
          // }
        }


        catch(err)
        {
          if(err.code = "invalid-argument"){
            this.showAlert("please fill StartTime , EndTime and interval with minimum of 15mins to generate TimeSlots ")
          }
          else{
            this.showAlert("error code is : "+err.code)
          }
        }
  
  }
 


  async showAlert(message : string)
  {
    const alert = await this.alertCtrl.create ({
      header:'Warning',
      message ,
      buttons:["OK"]
    })
    alert.present()
  }

}
