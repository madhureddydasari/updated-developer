import { Component, OnInit } from '@angular/core';
import { Doctorstruct } from '../modal/Docotor';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { FirebaseService } from '../services/firebase.service';
import { DivideTimeSlotService } from '../services/timeslots/divide-time-slot.service';

@Component({
  selector: 'app-update-doctor-slots',
  templateUrl: './update-doctor-slots.page.html',
  styleUrls: ['./update-doctor-slots.page.scss'],
})
export class UpdateDoctorSlotsPage implements OnInit {
  daysNames :string[]=["sun","mon","tue","wed","thu","fri","sat"]
  
  doctor : Doctorstruct ={
    // docId:'',
    name:'',
    department:'',
    description:'',
    phone: '',
    specialization: '',
    consultationFee : '',
    timings:'',
    facilities:'',
  }

  constructor(private router:Router,private alertCtrl:AlertController,private activatedRoute: ActivatedRoute , private fbServiceDoctor:FirebaseService,private fbtimeservice:DivideTimeSlotService) 
  { 
    const id=this.activatedRoute.snapshot.paramMap.get('id')
    this.fbServiceDoctor.getDoctor(id).subscribe((docData)=>{
      this.doctor=docData
    })
  }

  calculateAllLengths(){
    let startTimingsLength = 0     //To calculate Doctor's startTimings array length 
    let x=this.doctor.startTimings.forEach((element) => {
      if(element !="" && element!=null) { startTimingsLength++ } })
      // console.log(startTimingsLength);
      
    let endTimingsLength = 0     //To calculate Doctor's endTimings array length
    this.doctor.endTimings.forEach((element) => {
      if(element !="" && element!=null)  {  endTimingsLength++  }  })
    // console.log(endTimingsLength)

    let timingIntervalsLength = 0     //To calculate Doctor's timingIntervals array length
    this.doctor.timingIntervals.forEach((element) => {
      // console.log(element , typeof(element)) //to check if the element is of type string or number
      if(element!=null && element>=15)  {  timingIntervalsLength++  }  })
    // console.log(timingIntervalsLength)

    let arr:number[]=[startTimingsLength , endTimingsLength , timingIntervalsLength]
    return arr
  }
 
  async updateDoctorSlots()
  {

        try
        {
          let arr =this.calculateAllLengths();
          console.log(arr[0],arr[1],arr[2])          
          if(arr[0]==7 && arr[1]==7 && arr[2]==7)
          {
            this.doctor.timeSlots = this.fbtimeservice.divideTimeSlots(this.doctor.startTimings , this.doctor.endTimings,this.doctor.timingIntervals)
            // console.log(this.doctor.timeSlots[0].slots[0])
            // this.fbservice.enableDisableDoctor(this.doctor,this.enableOrDisable) 
            // no need of a SEPARATE piece of code as we merged that with the doctor struct and directly updating
            this.fbServiceDoctor.updateDoctorSlots(this.doctor).then(()=> {
              this.router.navigateByUrl(`/view-doctor/${this.doctor.id}`)
            });
          }
          else
          {
            this.showAlert("please fill StartTime , EndTime and interval with minimum of 15 mins to generate TimeSlots")
          }
        }


        catch(err)
        {
          if(err.code = "invalid-argument"){
            this.showAlert("Invalid Arguments")
            // this.showAlert("please fill StartTime , EndTime and interval with minimum of 15mins to generate TimeSlots ")
          }
          else{
            this.showAlert("error code is : "+err.code)
          }
        }
  
  }
 

  ngOnInit() {
  }


  showAlert(message:string)
  {
    this.alertCtrl.create({
      message,
      buttons:["ok"]
    }).then((alert) => {
      alert.present()
    })
  }
}

