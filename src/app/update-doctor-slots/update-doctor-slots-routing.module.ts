import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateDoctorSlotsPage } from './update-doctor-slots.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateDoctorSlotsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateDoctorSlotsPageRoutingModule {}
