export interface Doctorstruct {
        doctorExist ?:boolean; // to active or block the doctor
        id?: any;
        name: string;
        description : string;
        department : string;
        phone : string;
        specialization: string;
        consultationFee : string;
        timings:string;
        facilities: string;
        createdAt? :any;    // new Date().getTime()   // collects in milliseconds
        createdOn? :Date;    // new Date()    // collects entire date and can be viewed by convertng to date.toLocalDateString
        lastModified? :any ;  // 
        docId ?: any;
        doctuserUID?:string;  // doctors user UID
        emailId ?: string;
        password ?: string;
        dateRange?:number; // it is useful for a doctor to show dates available for booking on calendar using no.of days
        toInclude ?: boolean;  // Doctors Choice to apper or disapper himself to the user
        timeSlots?: any[];   // stores particular users timeslots
        startTimings?:string[];  // input taken from user of weekly startTimes
        endTimings?:string[]  ;    // input taken from user of weekly  endTimes
        timingIntervals?:number[]; // input taken from user of weekly  timeIntervals Between startTimes nd endTime
        // given in doctregister.ts "yes" by default while adding for first time
    } 

// export interface bookingstruct {
//     id?: any;
//     userID : string ;
//     doctorid : string ;
//     doctorname : string ;
//     status : string ;
// }