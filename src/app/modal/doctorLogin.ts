export interface DoctorLoginstruct {
    id?: any;
    name: string;
    description : string;
    department : string;
    phone : string;
    specialization: string;
    consultationFee : string;
    timings:string;
    facilities: string;
    createdAt :any;
    // docId : any;
    doctuserUID?:string;
    emailId : string;
    password : string;
    toInclude ?: string;

} 